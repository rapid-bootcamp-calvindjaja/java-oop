package Logic.Logic01;

import Logic.BasicLogic;

public class Logic01Soal02 extends BasicLogic {
    public Logic01Soal02(int n){
        super(n);
    }

    public void isiArray(){
        int deretPertama = 0;
        int deretKedua = 0;
        for (int i = 0; i < this.n; i++) {
            if (i % 2 == 0) {
                deretPertama++;
                this.array[0][i] = String.valueOf(deretPertama);
            } else {
                deretKedua += 3;
                this.array[0][i] = String.valueOf(deretKedua);
            }
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}

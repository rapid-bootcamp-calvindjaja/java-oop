package Logic.Logic01;

import Logic.BasicLogic;

public class Logic01Soal08 extends BasicLogic {
    public Logic01Soal08(int n){
        super(n);
    }

    public void isiArray(){
        int[] deret = new int[n];
        int bilPertama = 65;
        int bilKedua = 2;
        for (int i = 0; i < this.n; i++) {
            if(i % 2 == 1){
                deret[i] = bilPertama;
                bilPertama += 2;
                this.array[0][i]= String.valueOf((char) deret[i]);
            }
            else {
                deret[i] = bilKedua;
                bilKedua += 2;
                this.array[0][i]= String.valueOf(deret[i]);
            }


        }
    }

    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}

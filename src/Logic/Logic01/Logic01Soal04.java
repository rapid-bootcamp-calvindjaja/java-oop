package Logic.Logic01;

import Logic.BasicLogic;

public class Logic01Soal04 extends BasicLogic {
    public Logic01Soal04(int n){
        super(n);
    }

    public void isiArray(){
        int[] deret = new int[n];
        for (int i = 0; i < this.n; i++) {
            if (i <= 1) deret[i] = 1;
            else deret[i] = deret[i - 1] + deret[i - 2];
            this.array[0][i] = String.valueOf(deret[i]);
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}

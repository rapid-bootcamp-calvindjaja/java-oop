package Logic.Logic01;

import Logic.BasicLogic;

public class Logic01Soal10 extends BasicLogic {
    public Logic01Soal10(int n){
        super(n);
    }

    public void isiArray(){
        int angka = 1;
        for (int i = 0; i < this.n; i++) {
            this.array[0][i]= String.valueOf((int) Math.pow(i,3));
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}

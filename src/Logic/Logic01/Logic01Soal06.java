package Logic.Logic01;

import Logic.BasicLogic;

public class Logic01Soal06 extends BasicLogic {
    public Logic01Soal06(int n){
        super(n);
    }

    public void isiArray(){
        int flagArray = 0;
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            int bil = 0;
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    bil = bil + 1;
                }
            }
            if (bil == 2) {
                this.array[0][flagArray] = String.valueOf(i);
                flagArray++;
            }
            if (flagArray >= this.array.length) {
                break;
            }
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.printSingle();
    }
}
